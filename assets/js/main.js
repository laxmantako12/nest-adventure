jQuery(document).ready(function($) {

    // AOS Init
    AOS.init({
        once: true,
        offset: 200,
        duration: 800,
        delay: 100
    });

    //
    function addImgFluid() {
        $('img').addClass('img-fluid');
    }

    //Dropdown Menu
    function dropdownMenu() {
        if($(window).width()>320){
            $('.navbar .dropdown').hover(function() {
                $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();
        
            }, function() {
                $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();
        
            });
              $('.navbar .dropdown > a').click(function(){
                location.href = this.href;
            });
        
        }
    }

    // 
    function handleNavbarToggler() {
        $('body').on('click', '.navbar-toggler', function() {
            $(this).toggleClass('on');
            $('.navbar-collapse').toggleClass('shown');
            $('.navbar-collapse').slideToggle();
            $('.navbar').toggleClass('open');
        });
    }

    //
    function dropToggler() {
        $('body').on('click', '.drop-toggler', function() {
            $(this).parents('.menu-item-has-children').find('.sub-menu').slideToggle();
            $(this).toggleClass('open');
        });
    }

  
    //
    function matchHeights() {
        var options = {
            byRow: false,
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        };
        $('.section-article .article-wrap .content h4').matchHeight(options);
    }

 

    //
    function mainSlider() {
        $('.tour-slider').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 4000,
            arrows: true,
            dots: false,
            // stagePadding: 200,
            centerMode: true,
            centerPadding: '100px',
            prevArrow: '<button type="button" class="slick-prev"></button>',
            nextArrow: '<button type="button" class="slick-next"></button>',
            responsive: [
                {
                    breakpoint: 1800,
                    settings: {
                        centerMode: true,
                        centerPadding: '150px',
                        slidesToShow: 3
                    }
                },
                {
                breakpoint: 1500,
                settings: {
                    centerMode: true,
                    centerPadding: '150px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 1199,
                settings: {
                    centerMode: true,
                    centerPadding: '150px',
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 640,
                settings: {
                    centerMode: true,
                    centerPadding: '0',
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    centerMode: true,
                    centerPadding: '0',
                    slidesToShow: 1
                }
            }
            ]
        });


        // 
        $('.testimonial ').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 4000,
            arrows: true,
            dots: false,
            // stagePadding: 200,
            centerMode: true,
            centerPadding: '310px',
            prevArrow: '<button type="button" class="slick-prev"></button>',
            nextArrow: '<button type="button" class="slick-next"></button>',
            responsive: [{
                breakpoint: 1500,
                settings: {
                    centerMode: true,
                    centerPadding: '100px',
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 1199,
                settings: {
                    centerMode: true,
                    centerPadding: '100px',
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 640,
                settings: {
                    centerMode: true,
                    centerPadding: '0',
                    slidesToShow: 1
                }
            }
            ]
            
        });

    }

    // 
    function prependSliderArrow() {
        $('.tour-slider .slick-arrow').prependTo('.slick-arrows-1');
        $('.testimonial .slick-arrow').prependTo('.slick-arrows-2');
    }

    // addImgFluid();
    dropdownMenu();
    handleNavbarToggler();
    dropToggler();
    matchHeights();
    mainSlider();
    prependSliderArrow();


});

// 
$(window).on("load resize", function (e) {
    var containeroffset = $('.footer .container').offset().left;
    $('.section-tour .container').css({
        'padding-left': containeroffset,
    })
    // 
    $('.section-welcome .container').css({
        'padding-right': containeroffset,
    })
    $('.section-testimonial .container').css({
        'padding-left': containeroffset,
    })
});
